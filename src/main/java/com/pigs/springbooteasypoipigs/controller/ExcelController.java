package com.pigs.springbooteasypoipigs.controller;

import com.pigs.springbooteasypoipigs.entity.UserEntity;
import com.pigs.springbooteasypoipigs.utils.EasyPoiUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author PIGS
 * @version 1.0
 * @date 2020/4/25 14:22
 * @effect :
 * Excel 前端控制器
 */
@RestController
public class ExcelController {

    /**
     * 从表格插入数据
     * 接收并返回前台
     *
     * @param file
     * @return
     * @throws IOException
     */
    @RequestMapping("/uploadExcel")
    public Map<String,Object> uploadExcel(@RequestParam("file") MultipartFile file) throws IOException {
        List<UserEntity> checkingIns = EasyPoiUtils.importExcel(file, UserEntity.class);
        Map<String,Object> map = new HashMap<>();
        map.put("code",200);
        map.put("msg","ok");
        map.put("data",checkingIns);
        return map;
    }
}
