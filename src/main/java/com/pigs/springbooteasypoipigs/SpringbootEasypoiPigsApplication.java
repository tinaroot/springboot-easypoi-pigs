package com.pigs.springbooteasypoipigs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootEasypoiPigsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootEasypoiPigsApplication.class, args);
    }

}
